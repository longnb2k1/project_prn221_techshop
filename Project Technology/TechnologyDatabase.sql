﻿IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'Technology_Management')
BEGIN
    ALTER DATABASE Demo SET SINGLE_USER WITH ROLLBACK IMMEDIATE
    DROP DATABASE Demo
END
CREATE DATABASE Demo
GO
USE Demo
-- Tạo bảng Account
CREATE TABLE Account (
    UserID INT IDENTITY(1,1) PRIMARY KEY,
    Name NVARCHAR(200),
    Username VARCHAR(50) NOT NULL,
    Password VARCHAR(50) NOT NULL,
    Email VARCHAR(100),
    PhoneNumber VARCHAR(20),
    Address NVARCHAR(2000),
    Role INT
)

-- Tạo bảng AccountDetail
CREATE TABLE AccountDetail (
    ID INT IDENTITY(1,1) PRIMARY KEY,
    UserID INT REFERENCES Account(UserID),
    Permission VARCHAR(15)
)

-- Tạo bảng Branch
CREATE TABLE Branch (
    Code VARCHAR(10) PRIMARY KEY,
    Name NVARCHAR(100),
    Country NVARCHAR(200)
)

-- Tạo bảng Category
CREATE TABLE Category (
    Code VARCHAR(10) PRIMARY KEY,
    Name NVARCHAR(100),
    SubCatCode VARCHAR(10) REFERENCES Category(Code),
    TT INT
)

-- Tạo bảng Product
CREATE TABLE Product (
    ID INT IDENTITY(1,1) PRIMARY KEY,
    ProductName NVARCHAR(2000),
    Price MONEY,
    Discount MONEY,
    Stock INT,
    BranchCode VARCHAR(10) REFERENCES Branch(Code),
    CategoryCode VARCHAR(10) REFERENCES Category(Code)
)

-- Tạo bảng Ability
CREATE TABLE Ability (
    ID INT IDENTITY(1,1) PRIMARY KEY,
    Name NVARCHAR(200)
)

-- Tạo bảng AbilityProduct
CREATE TABLE AbilityProduct (
    ProductID INT REFERENCES Product(ID),
    AbilityID INT REFERENCES Ability(ID),
    PRIMARY KEY (ProductID, AbilityID)
)

-- Tạo bảng Cart
CREATE TABLE Cart (
    AccountID INT REFERENCES Account(UserID),
    ProductID INT REFERENCES Product(ID),
    Quantity INT,
    DateAdd DATE,
    PRIMARY KEY (AccountID, ProductID)
)

-- Tạo bảng Order
CREATE TABLE [Order] (
    ID INT IDENTITY(1,1) PRIMARY KEY,
    CustomerID INT REFERENCES Account(UserID),
    CustomerName NVARCHAR(200),
    StaffID INT REFERENCES Account(UserID),
    Address NVARCHAR(2000),
    PhoneNumber VARCHAR(15),
    Discount MONEY,
    TotalPrice MONEY,
    DateOrder DATE,
    DateDelivery DATE,
    DateRecipt DATE
)
CREATE TABLE [OrderDetail] (
    OrderID INT REFERENCES [Order](ID),
    ProductID INT REFERENCES Product(ID),
    Quantity int,
	Price money,
)
-- Chèn thông tin tài khoản admin
INSERT INTO Account (Name, Username, Password, Role)
VALUES ('Administrator', 'admin', 'admin', 2)

-- Chèn thông tin tài khoản staff
INSERT INTO Account (Name, Username, Password, Role)
VALUES ('Staff', 'staff', 'staff', 1)

-- Chèn thông tin tài khoản customer
INSERT INTO Account (Name, Username, Password, Role)
VALUES ('Customer', 'cus', 'cus', 0)