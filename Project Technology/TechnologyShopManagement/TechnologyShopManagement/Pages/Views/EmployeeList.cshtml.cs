using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TechnologyShopManagement.DBContext;
using TechnologyShopManagement.Model;

namespace TechnologyShopManagement.Pages.Views
{
    public class EmployeeListModel : PageModel
    {
        private readonly AppDBContext _dbContext;
        public IEnumerable<Employees> employeesList { get; set; }
        public EmployeeListModel(AppDBContext db) {
            _dbContext = db;
        }
        public void OnGet()
        {
            employeesList = new List<Employees>();  
            employeesList= _dbContext.Employees;
        }
    }
}
