using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TechnologyShopManagement.DBContext;
using TechnologyShopManagement.Model;

namespace TechnologyShopManagement.Pages.Views
{
    public class ProductListModel : PageModel
    {
        private readonly AppDBContext _dbContext;
        public IEnumerable<Products> productsList { get; set; }
        public ProductListModel(AppDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void OnGet()
        {
            productsList=new List<Products>();
            productsList=_dbContext.Products;
        }
    }
}
