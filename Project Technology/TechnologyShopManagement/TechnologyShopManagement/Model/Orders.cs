﻿using System.Numerics;
using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;

namespace TechnologyShopManagement.Model
{
    public class Orders
    {
        [Key]
        public int order_id { get; set; }
        public int customer_id { get; set; }
        public string phone { get; set; }
        public int delivery_way { get; set; }
        public DateTime order_time { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal total_price { get; set; }
        public int order_status { get; set; }

    }
}
