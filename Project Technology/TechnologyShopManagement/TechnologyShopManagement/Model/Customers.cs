﻿using System.ComponentModel.DataAnnotations;
using System.Numerics;

namespace TechnologyShopManagement.Model
{
    public class Customers
    {
        [Key]
       public int customer_id { get; set; }
        public string customer_name { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public DateTime dob { get; set; }
    }
}
