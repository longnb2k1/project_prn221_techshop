﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TechnologyShopManagement.Model
{
    public class Order_details
    {
        [Key]
        public int order_detail_id { get; set; }
        public int order_id { get; set; }
        public int product_id { get; set; }
        public int quantity { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal amount_price { get; set; }



    }
}
