﻿using System.ComponentModel.DataAnnotations;
using System.Numerics;

namespace TechnologyShopManagement.Model
{
    public class Employees
    {
        [Key]
        public int employee_id { get; set; }
        public string email { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string employee_name { get; set; }
        public string phone { get; set; }
        public DateTime dob { get; set; }
        public DateTime started_date { get; set; }
        public int role { get; set; }
    }
}