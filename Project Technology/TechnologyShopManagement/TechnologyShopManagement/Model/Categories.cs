﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace TechnologyShopManagement.Model
{
    
    public class Categories
    {

        [Key]
      public int category_id { get; set; }
        [Required]
        public string category_name { get; set; }
        [Required]
        public int category_type { get; set; }
    }
}
