﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TechnologyShopManagement.Model
{
    public class Products
    {
        [Key]
        public int product_id { get; set; }
        public string product_name { get; set; }
        public int product_type { get; set; }
        public string description { get; set; }
        public string img { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal price { get; set; }
        public int quantity { get; set; }


    }
}
