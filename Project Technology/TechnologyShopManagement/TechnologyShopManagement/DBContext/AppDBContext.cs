﻿using Microsoft.EntityFrameworkCore;
using TechnologyShopManagement.Model;

namespace TechnologyShopManagement.DBContext
{
    public class AppDBContext : DbContext
    {
        public AppDBContext(DbContextOptions<AppDBContext>options) : base(options) { 
        }
        public DbSet<Categories> Categories { get; set; }
        public DbSet<Customers> Customers { get; set; } 
        public DbSet<Employees> Employees { get; set; } 
        public DbSet<Order_details> Order_details { get; set; }
        public DbSet<Orders> Orders { get; set; }
        public DbSet<Products> Products { get; set; }
    }
}
